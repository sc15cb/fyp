from app import db

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), index=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    def __repr__(self):
        return '<User {}>'.format(self.email)

# Used for Example 2 - SQL based on 1=1 Always True
class Sql_db_1(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), index=True)
    
    def __repr__(self):
        return '{}'.format(self.name)

# Used for Example 3 - SQL based on ""="" is Always True
class Sql_db_2(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(40), index=True)
    password = db.Column(db.String(40), index=True)
    
    def __repr__(self):
        return '{} {}'.format(self.username, self.password)

class Progression(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    sql_prog = db.Column(db.Integer)
    xss_prog = db.Column(db.Integer)
    csrf_prog = db.Column(db.Integer)
    lessons = db.Column(db.Integer)
    xp = db.Column(db.Integer)
    level = db.Column(db.Integer)
    wallet = db.Column(db.Integer)
    bg1 = db.Column(db.Integer)
    bg2 = db.Column(db.Integer)
    bg3 = db.Column(db.Integer)
    bg4 = db.Column(db.Integer)
    bg5 = db.Column(db.Integer)
    bg6 = db.Column(db.Integer)
    bg7 = db.Column(db.Integer)
    bg8 = db.Column(db.Integer)
    bg9 = db.Column(db.Integer)