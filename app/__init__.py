from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import Config
from flask_session import Session
from flask_caching import Cache

app = Flask(__name__)
sess = Session()
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)
app.config['SECRET_KEY'] = 'redsfsfsfsfis'
app.config['SESSION_TYPE'] = 'filesystem'
 # cache = Cache(app, config={'CACHE_TYPE': 'null'})
sess.init_app(app)

from app import models, views
