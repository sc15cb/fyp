from app import app
from flask import render_template, flash, request
from app import db
from app.models import User, Sql_db_1, Progression, Sql_db_2
import sqlite3 as sql
from flask_login import LoginManager, login_user

user = None
username = None

# accounts 
acc_1 = 0
acc_2 = 0
acc_3 = 0

# View the homepage
@app.route('/')
def index():
    global user
    print(user)
    return render_template('index.html', user=user)

# View the hub page
@app.route('/hub', methods = ['POST', 'GET'])
def hubPage():
    global user
    global username
    if user == None:
        flash("Please log in to view the hub page.")
        return render_template('login.html', user=user)

    else:
        
        bg4 = None
        bg5 = None
        bg6 = None
        bg7 = None
        bg8 = None
        bg9 = None
        level = None
        

        if user != None:
            bb = Progression.query.filter_by(user_id=user.id).first()
            lessons = bb.lessons
            wallet = bb.wallet

            if bb.lessons > 0:
                aa = Progression.query.filter_by(user_id=user.id).update(dict(bg4=1))
                aa = Progression.query.filter_by(user_id=user.id).update(dict(bg6=1))
                bb = Progression.query.filter_by(user_id=user.id).first()
                bg4 = bb.bg4
                bg6 = bb.bg6

            if bb.lessons > 4:
                aa = Progression.query.filter_by(user_id=user.id).update(dict(bg5=1))
                aa = Progression.query.filter_by(user_id=user.id).update(dict(level=1))
                bb = Progression.query.filter_by(user_id=user.id).first()
                bg5 = bb.bg5
                level = bb.level

            if bb.lessons > 1:
                aa = Progression.query.filter_by(user_id=user.id).update(dict(bg7=1))
                bb = Progression.query.filter_by(user_id=user.id).first()
                bg7 = bb.bg7

            if bb.lessons > 2:
                aa = Progression.query.filter_by(user_id=user.id).update(dict(bg8=1))
                bb = Progression.query.filter_by(user_id=user.id).first()
                bg8 = bb.bg8

            if bb.lessons > 12:
                aa = Progression.query.filter_by(user_id=user.id).update(dict(bg9=1))
                bb = Progression.query.filter_by(user_id=user.id).first()
                bg9 = bb.bg9

            if bb.lessons > 9:
                aa = Progression.query.filter_by(user_id=user.id).update(dict(level=2))
                bb = Progression.query.filter_by(user_id=user.id).first()
                level = bb.level

            if bb.lessons > 14:
                aa = Progression.query.filter_by(user_id=user.id).update(dict(level=3))
                bb = Progression.query.filter_by(user_id=user.id).first()
                level = bb.level

            if bb.lessons > 19:
                aa = Progression.query.filter_by(user_id=user.id).update(dict(level=4))
                bb = Progression.query.filter_by(user_id=user.id).first()
                level = bb.level

            if bb.lessons > 24:
                aa = Progression.query.filter_by(user_id=user.id).update(dict(level=5))
                bb = Progression.query.filter_by(user_id=user.id).first()
                level = bb.level
                


            return render_template('hub.html', user=user, username=user.name, lessons=lessons, wallet=wallet, bg4=bg4, bg5=bg5, bg6=bg6, bg7=bg7, bg8=bg8, bg9=bg9, level=level)

    return render_template('hub.html', user=user, username=user.name)

# View the lessons page
@app.route('/lessons', methods = ['POST', 'GET'])
def lessons():
    global user
    sql = None
    xss = None
    csrf = None

    if user == None:
        flash("Please log in to view the lessons page.")
        return render_template('login.html', user=user)

    else:
        # get the progression object
        u = Progression.query.filter_by(user_id=user.id).first()
        db.session.commit()

        if u != None:
            sql = u.sql_prog
            xss = u.xss_prog
            csrf = u.csrf_prog

        return render_template('lessons.html', user=user, sql=sql, xss=xss, csrf=csrf)
    
# View the login page
@app.route('/login', methods = ['POST', 'GET'])
def login():
    global user
    return render_template('login.html', user=user)

# Login the user functionality
@app.route('/login_user', methods = ['POST', 'GET'],)
def login_user():
    global user
    if request.method=='POST':

        # Get the request
        email = request.form['email']
        password = request.form['password']

        # Check if emnail already exists
        u = User.query.filter_by(email=email, password_hash=password).first()
        print(u)
        if u != None:
            user = u
            # Notify user and return the webpage
            return render_template('index.html', user=user)

        else:
            flash("Could not log in, Please input a valid email and password")
            return render_template('login.html', user=user)

    if request.method=='POST':

        # Get the request
        email = request.form['email']
        password = request.form['password']

        # Check if emnail already exists
        user = User.query.filter_by(email=email, password_hash=password)
        if user != "":
            print(user)
            # Notify user and return the webpage
            flash("You logged in successfully redirecting you.")
            return render_template('login.html', user=user)

        else:
            flash("Could not log in, Please input a valid email and password")
            return render_template('login.html', user=user)

# Register the user functionality
@app.route('/register_user', methods = ['POST', 'GET'])
def register_user():

    if request.method=='POST':

        # Get the request
        name = request.form['name']
        email = request.form['email']
        password = request.form['password']

        # Check if emnail already exists
        p = User.query.filter_by(email=email).first()
        print(p)
        if p != None:
            flash("Sorry that email already exists, please try again.")
            return render_template('login.html', user=user)

        else:

            # Add the user to the database
            u = User(name=name, email=email, password_hash=password)
            db.session.add(u)
            db.session.commit()

            # Get user id
            user_id = User.query.filter_by(email=email).first()
            print (user_id)
            user_id = user_id.id

            # Add the progression to the db
            proggers = Progression(user_id=user_id, sql_prog=0, xss_prog=0, csrf_prog=0, lessons=0, xp=0, wallet=0, level=0, bg1=1,
            bg2=0, bg3=0, bg4=0, bg5=0, bg6=0, bg7=0, bg8=0, bg9=0)
            db.session.add(proggers)
            db.session.commit()

            # Notify user and return the webpage
            flash("Your account has been registered, Please log in now.")
            return render_template('login.html', user=user)

# Logout user functionality
@app.route('/logout_user', methods = ['POST', 'GET'])
def logout():
    global user 
    user = None
    flash("You have successfully logged out")
    return render_template('login.html', user=user)


####  SQL Injection Lessons ####

# SQL Lesson 1 page.
@app.route('/sql_lesson', methods = ['POST', 'GET'])
def sql_lesson_1():
    global user
    return render_template('sql_lesson.html', user=user)


# SQL Lesson 2 page.
@app.route('/sql_lesson2', methods = ['POST', 'GET'])
def sql_lesson_2():
    global user

    if user != None:
        aa = Progression.query.filter_by(user_id=user.id).update(dict(sql_prog=1))
        bb = Progression.query.filter_by(user_id=user.id).first()
        aa = Progression.query.filter_by(user_id=user.id).update(dict(lessons=bb.lessons+1))
        db.session.commit()

    # First delete all entries of the table

    db.session.query(Sql_db_1).delete()
    db.session.commit()

    # First create the table and add the entries
    u = Sql_db_1(name="Jesse Pinkman")
    db.session.add(u)
    u = Sql_db_1(name="Logan")
    db.session.add(u)
    u = Sql_db_1(name="Deadpool")
    db.session.add(u)
    u = Sql_db_1(name="Jimmy Mcgil")
    db.session.add(u)
    db.session.commit()

    return render_template('sql_lesson_2.html', user=user)

# SQL Lesson 2 - find username button
@app.route('/sql_lesson2_test', methods = ['POST', 'GET'])
def sql_lesson_2_test():
    global user

    userID = request.form['userID']
    print(userID)

    # Sqlite stuff
    con = sql.connect('app.db')
    cur = con.cursor()
    cur.execute("SELECT * FROM Sql_db_1 WHERE id = %s" % userID)
    results = cur.fetchall();
    print(results)
    con.close()

    return render_template('sql_lesson_2.html', user=user, results=results)

# SQL Lesson 3 page
@app.route('/sql_lesson3', methods = ['POST', 'GET'])
def sql_lesson_3():
    global user

    if user != None:
        aa = Progression.query.filter_by(user_id=user.id).update(dict(sql_prog=2))
        bb = Progression.query.filter_by(user_id=user.id).first()
        aa = Progression.query.filter_by(user_id=user.id).update(dict(lessons=bb.lessons+1))
        db.session.commit()

    # First delete all entries of the table

    db.session.query(Sql_db_2).delete()
    db.session.commit()

    # First create the table and add the entries
    u = Sql_db_2(username="testuser", password="password")
    db.session.add(u)
    u = Sql_db_2(username="admin", password="harlsey")
    db.session.add(u)
    u = Sql_db_2(username="chris", password="securepassword123")
    db.session.add(u)
    u = Sql_db_2(username="Jimmy Mcgil", password="breakingbadfan")
    db.session.add(u)
    db.session.commit()
    db.session.close()

    return render_template('sql_lesson_3.html', user=user)

# SQL lesson 3 login button
@app.route('/sql_lesson3_test', methods = ['POST', 'GET'])
def sql_lesson_3_test():
    global user

    username = request.form['username']
    password = request.form['password']
    print(username)
    print(password)

    # Sqlite stuff
    con = sql.connect('app.db')
    cur = con.cursor()
    cur.execute("SELECT * FROM Sql_db_2 WHERE username = '%s' and password = '%s'" % (username, password))

    results = cur.fetchall();
    print(results)
    con.close()
    return render_template('sql_lesson_3.html', user=user, results=results)


# SQL Lesson 4 page
@app.route('/sql_lesson4', methods = ['POST', 'GET'])
def sql_lesson_4():
    global user

    if user != None:
        aa = Progression.query.filter_by(user_id=user.id).update(dict(sql_prog=3))
        bb = Progression.query.filter_by(user_id=user.id).first()
        aa = Progression.query.filter_by(user_id=user.id).update(dict(lessons=bb.lessons+1))
        db.session.commit()

    # Sqlite stuff
    con = sql.connect('app.db')
    cur = con.cursor()
    table = """ CREATE TABLE IF NOT EXISTS Sql_db_2 (
    id integer PRIMARY KEY,
    username text NOT NULL,
    password text NOT NULL
    ); """;
    cur.execute(table);
    con.commit(); # Save table into database.

    # First delete all entries of the table

    db.session.query(Sql_db_2).delete()
    db.session.commit()

    # First create the table and add the entries
    u = Sql_db_2(username="testuser", password="password")
    db.session.add(u)
    u = Sql_db_2(username="admin", password="harlsey")
    db.session.add(u)
    u = Sql_db_2(username="chris", password="securepassword123")
    db.session.add(u)
    u = Sql_db_2(username="Jimmy Mcgil", password="breakingbadfan")
    db.session.add(u)
    db.session.commit()
    db.session.close()
    return render_template('sql_lesson_4.html', user=user)

# SQL lesson 4 login button
@app.route('/sql_lesson4_test', methods = ['POST', 'GET'])
def sql_lesson_4_test():

    db.session.query(Sql_db_2).delete()
    db.session.commit()

    # First create the table and add the entries
    u = Sql_db_2(username="testuser", password="password")
    db.session.add(u)
    u = Sql_db_2(username="admin", password="harlsey")
    db.session.add(u)
    u = Sql_db_2(username="chris", password="securepassword123")
    db.session.add(u)
    u = Sql_db_2(username="Jimmy Mcgil", password="breakingbadfan")
    db.session.add(u)
    db.session.commit()
    db.session.close()

        # Sqlite stuff
    con = sql.connect('app.db')
    cur = con.cursor()

    global user

    userid = request.form['userid']
    print(userid)
    results = ""

    # Prevent user deleting database of user and progression
    if "DROP TABLE" in userid:
        if "DROP TABLE sql_db_1;" in userid:
            userID = "6969" 
    
        elif "DROP TABLE Progression;" in userid:
            userid = "0161" 
        
        # Success
        elif "DROP TABLE Users;" in userid:
            userid = "0001" 

        elif "DROP TABLE User;" in userid:
            userid = "0002" 

    # Detect if it is a batch
    elif " " in userid or ";" in userid:
        # prevent it forcefully.
        userid = "0"
        cur.executescript("SELECT * FROM Sql_db_2 WHERE id = " + (userid))
        results = cur.fetchall();

    # Normal statement
    else:
        cur.execute("SELECT * FROM Sql_db_2 WHERE id = '%s'" % (userid))
        results = cur.fetchall();

    # Check for success.
    if userid is "0001":
        results = "Well done you successfully deleted the database!"
    elif userid is "6969":
        results = "Nice try but I'm the teacher here."
    elif userid is "0161":
        results = "Nice try but I'm the teacher here."
    elif userid is "0002":
        results = "Nice try but I'm the teacher here."

    con.close()

    return render_template('sql_lesson_4.html', user=user, results=results)

# SQL Lesson 5 page.
@app.route('/sql_lesson5', methods = ['POST', 'GET'])
def xss_lesson_5():
    global user
    if user != None:
        aa = Progression.query.filter_by(user_id=user.id).update(dict(sql_prog=4))
        bb = Progression.query.filter_by(user_id=user.id).first()
        aa = Progression.query.filter_by(user_id=user.id).update(dict(lessons=bb.lessons+1))
        db.session.commit()
    return render_template('sql_lesson_5.html', user=user)

# SQL Lesson 5 page.
@app.route('/sql_lesson6', methods = ['POST', 'GET'])
def xss_lesson_6():
    global user
    sql = None
    xss = None
    csrf = None
    if user != None:
        aa = Progression.query.filter_by(user_id=user.id).update(dict(sql_prog=5))
        bb = Progression.query.filter_by(user_id=user.id).first()
        aa = Progression.query.filter_by(user_id=user.id).update(dict(lessons=bb.lessons+1))
        db.session.commit()

        # get the progression object
        u = Progression.query.filter_by(user_id=user.id).first()
        db.session.commit()
        sql = u.sql_prog
        xss = u.xss_prog
        csrf = u.csrf_prog

    return render_template('lessons.html', user=user, sql=sql, xss=xss, csrf=csrf)

####  XSS Cross site Scripting Lessons ####

# XSS Lesson 1 page.
@app.route('/xss_lesson', methods = ['POST', 'GET'])
def xss_lesson_1():
    global user
    return render_template('XSS_lesson.html', user=user)

# XSS Lesson 1 Button.
@app.route('/xss_lesson_test', methods = ['POST', 'GET'])
def xss_lesson_test():

    if user != None:
        aa = Progression.query.filter_by(user_id=user.id).update(dict(xss_prog=1))
        db.session.commit()

    output = request.form['text']
    print(output)
    return render_template('XSS_lesson.html', user=user, output=output)

# XSS Lesson 2 page.
@app.route('/xss_lesson2', methods = ['POST', 'GET'])
def xss_lesson_2():
    global user

    if user != None:
        aa = Progression.query.filter_by(user_id=user.id).update(dict(xss_prog=1))
        bb = Progression.query.filter_by(user_id=user.id).first()
        aa = Progression.query.filter_by(user_id=user.id).update(dict(lessons=bb.lessons+1))
        db.session.commit()

    return render_template('XSS_lesson_2.html', user=user)

# XSS Lesson 2 Button - Styles
@app.route('/xss_lesson_2_test', methods = ['POST', 'GET'])
def xss_lesson_test_styles():
    output = request.form['text']
    print(output)
    return render_template('XSS_lesson_2.html', user=user, output=output)
    
# XSS Lesson 2 Button - JavaScript
@app.route('/xss_lesson_2_test_2', methods = ['POST', 'GET'])
def xss_lesson_test_js():
    output2 = request.form['text2']
    print(output2)
    return render_template('XSS_lesson_2.html', user=user, output2=output2)

# XSS Lesson 3 page.
@app.route('/xss_lesson3', methods = ['POST', 'GET'])
def xss_lesson_3():
    global user
    if user != None:
        aa = Progression.query.filter_by(user_id=user.id).update(dict(xss_prog=2))
        bb = Progression.query.filter_by(user_id=user.id).first()
        aa = Progression.query.filter_by(user_id=user.id).update(dict(lessons=bb.lessons+1))
        db.session.commit()

    car_name = None
    return render_template('XSS_lesson_3.html', user=user, car_name=car_name)

# XSS Lesson 3 create listing button
@app.route('/xss_lesson_3_test', methods = ['POST', 'GET'])
def xss_lesson_3_test():
    global user
    car_name = request.form['name']
    car_description = request.form['des']
    return render_template('XSS_lesson_3.html', user=user, car_name=car_name, car_description=car_description)

# XSS Lesson 4 page.
@app.route('/xss_lesson4', methods = ['POST', 'GET'])
def xss_lesson_4():
    global user

    if user != None:
        aa = Progression.query.filter_by(user_id=user.id).update(dict(xss_prog=3))
        bb = Progression.query.filter_by(user_id=user.id).first()
        aa = Progression.query.filter_by(user_id=user.id).update(dict(lessons=bb.lessons+1))
        db.session.commit()

    return render_template('XSS_lesson_4.html', user=user)

# XSS Lesson 5 page.
@app.route('/xss_lesson5', methods = ['POST', 'GET'])
def xss_lesson_done():
    global user

    sql = None
    xss = None
    csrf = None
    if user != None:
        aa = Progression.query.filter_by(user_id=user.id).update(dict(xss_prog=4))
        bb = Progression.query.filter_by(user_id=user.id).first()
        aa = Progression.query.filter_by(user_id=user.id).update(dict(lessons=bb.lessons+1))
        db.session.commit()

        # get the progression object
        u = Progression.query.filter_by(user_id=user.id).first()
        db.session.commit()
        sql = u.sql_prog
        xss = u.xss_prog
        csrf = u.csrf_prog
        
    return render_template('lessons.html', user=user, sql=sql, xss=xss, csrf=csrf)

####  Cross-site Request Forgery (CSRF) lessons ####

# CSRF Lesson 1 page.
@app.route('/csrf_lesson', methods = ['POST', 'GET'])
def csrf_lesson1():

    global user
    return render_template('csrf_lesson.html', user=user)

# CSRF Lesson 2 page.
@app.route('/csrf_lesson2', methods = ['POST', 'GET'])
def csrf_lesson2():
    global user
    if user != None:
        aa = Progression.query.filter_by(user_id=user.id).update(dict(csrf_prog=1))
        bb = Progression.query.filter_by(user_id=user.id).first()
        aa = Progression.query.filter_by(user_id=user.id).update(dict(lessons=bb.lessons+1))
        db.session.commit()

    

    # The accounts
    global acc_1
    global acc_2
    global acc_3
    acc_1 = 100
    acc_2 = 0
    acc_3 = 0

    return render_template('csrf_lesson2.html',user=user, acc_1=acc_1, acc_2=acc_2, acc_3=acc_3)

# CSRF Lesson 2 page Send money button.
@app.route('/csrf_lesson2_send', methods = ['POST', 'GET'])
def csrf_send_money():
    global user
    global acc_1
    global acc_2
    global acc_3

    # get form
    acc_num = request.form['acc_num']
    amount = request.form['amount']

    print(acc_num)
    print(amount)

    # validation
    if acc_num is "1":
       flash("Bad - You can't send money to yourself") 

    elif acc_num is "2":
        # Check amount is Good
        if int(amount) > int(acc_1):
            flash("Bad - You have insufficient funds to make this transfer")
        else:
            int(acc_2)
            acc_2 += int(amount)
            acc_1 -= int(amount)
            flash("Good - money has been transfered.")



    elif acc_num is "3":
        # Check amount is Good
        if int(amount) > int(acc_1):
            flash("Bad - You have insufficient funds to make this transfer")
        else:
            acc_3 += int(amount)
            acc_1 -= int(amount)
            flash("Good - money has been transfered.")

    else:
       flash("Bad - Not a valid account number. look at the results table for more information on accounts.") 

    print("account 1" + str(acc_1))
    print("account 2" + str(acc_2))
    print("account 3" + str(acc_3))

    return render_template('csrf_lesson2.html', user=user, acc_1=acc_1, acc_2=acc_2, acc_3=acc_3)


# CSRF Lesson 3 page.
@app.route('/csrf_lesson3', methods = ['POST', 'GET'])
def csrf_lesson3():

    global user

    if user != None:
        aa = Progression.query.filter_by(user_id=user.id).update(dict(csrf_prog=2))
        bb = Progression.query.filter_by(user_id=user.id).first()
        aa = Progression.query.filter_by(user_id=user.id).update(dict(lessons=bb.lessons+1))
        db.session.commit()

    return render_template('csrf_lesson3.html', user=user)

# CSRF Lesson 3 page.
@app.route('/csrf_lesson4', methods = ['POST', 'GET'])
def csrf_lesson4():
    global user
    sql = None
    xss = None
    csrf = None
    if user != None:
        aa = Progression.query.filter_by(user_id=user.id).update(dict(csrf_prog=3))
        bb = Progression.query.filter_by(user_id=user.id).first()
        aa = Progression.query.filter_by(user_id=user.id).update(dict(lessons=bb.lessons+1))
        db.session.commit()

        # get the progression object
        u = Progression.query.filter_by(user_id=user.id).first()
        db.session.commit()
        sql = u.sql_prog
        xss = u.xss_prog
        csrf = u.csrf_prog


    return render_template('lessons.html', user=user, sql=sql, xss=xss, csrf=csrf)